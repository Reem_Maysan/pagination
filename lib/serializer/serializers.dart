import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:paginationapi/models/base/base_response.dart';
import 'package:paginationapi/models/images_model/images_model.dart';
import 'package:paginationapi/models/product_model/base_products.dart';
import 'package:paginationapi/models/product_model/product_model.dart';

part 'serializers.g.dart';

//add all of the built value types that require serialization
@SerializersFor(const [ProductModel, ImagesModel, BaseProducts, BaseResponse])

// normal serialization
//final Serializers serializers = _$serializers

// built_value has it's own json serialization, we can create valid typical json like this:

final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(ProductModel),
            ],
          )),
          () => ListBuilder<ProductModel>())
      ..addBuilderFactory(
          (FullType(
            BaseResponse,
            [
              const FullType(ProductModel),
            ],
          )),
          () => BaseResponseBuilder<ProductModel>())
      ..addBuilderFactory(
          (FullType(
            BaseResponse,
            [
              FullType(
                BuiltList,
                [
                  const FullType(ProductModel),
                ],
              ),
            ],
          )),
          () => BaseResponseBuilder<BuiltList<ProductModel>>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(ImagesModel),
            ],
          )),
          () => ListBuilder<ImagesModel>())
      ..addBuilderFactory(
          (FullType(
            BaseResponse,
            [
              const FullType(BaseProducts),
            ],
          )),
          () => BaseResponseBuilder<BaseProducts>()))
    .build();
