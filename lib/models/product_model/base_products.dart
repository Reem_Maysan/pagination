library base_products;

import 'dart:convert';

import 'package:paginationapi/models/product_model/product_model.dart';
import 'package:paginationapi/serializer/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_products.g.dart';

abstract class BaseProducts implements Built<BaseProducts, BaseProductsBuilder> {
  // fields go here

  @BuiltValueField(wireName:"last_page")
  int get page ;

  BuiltList<ProductModel> get data;

  BaseProducts._();

  factory BaseProducts([updates(BaseProductsBuilder b)]) = _$BaseProducts;

  String toJson() {
    return json.encode(serializers.serializeWith(BaseProducts.serializer, this));
  }

  static BaseProducts fromJson(String jsonString) {
    return serializers.deserializeWith(BaseProducts.serializer, json.decode(jsonString));
  }

  static Serializer<BaseProducts> get serializer => _$baseProductsSerializer;
}