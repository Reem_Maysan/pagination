// GENERATED CODE - DO NOT MODIFY BY HAND

part of base_products;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BaseProducts> _$baseProductsSerializer =
    new _$BaseProductsSerializer();

class _$BaseProductsSerializer implements StructuredSerializer<BaseProducts> {
  @override
  final Iterable<Type> types = const [BaseProducts, _$BaseProducts];
  @override
  final String wireName = 'BaseProducts';

  @override
  Iterable<Object> serialize(Serializers serializers, BaseProducts object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'last_page',
      serializers.serialize(object.page, specifiedType: const FullType(int)),
      'data',
      serializers.serialize(object.data,
          specifiedType:
              const FullType(BuiltList, const [const FullType(ProductModel)])),
    ];

    return result;
  }

  @override
  BaseProducts deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BaseProductsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'last_page':
          result.page = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'data':
          result.data.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProductModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$BaseProducts extends BaseProducts {
  @override
  final int page;
  @override
  final BuiltList<ProductModel> data;

  factory _$BaseProducts([void Function(BaseProductsBuilder) updates]) =>
      (new BaseProductsBuilder()..update(updates)).build();

  _$BaseProducts._({this.page, this.data}) : super._() {
    if (page == null) {
      throw new BuiltValueNullFieldError('BaseProducts', 'page');
    }
    if (data == null) {
      throw new BuiltValueNullFieldError('BaseProducts', 'data');
    }
  }

  @override
  BaseProducts rebuild(void Function(BaseProductsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BaseProductsBuilder toBuilder() => new BaseProductsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BaseProducts && page == other.page && data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, page.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BaseProducts')
          ..add('page', page)
          ..add('data', data))
        .toString();
  }
}

class BaseProductsBuilder
    implements Builder<BaseProducts, BaseProductsBuilder> {
  _$BaseProducts _$v;

  int _page;
  int get page => _$this._page;
  set page(int page) => _$this._page = page;

  ListBuilder<ProductModel> _data;
  ListBuilder<ProductModel> get data =>
      _$this._data ??= new ListBuilder<ProductModel>();
  set data(ListBuilder<ProductModel> data) => _$this._data = data;

  BaseProductsBuilder();

  BaseProductsBuilder get _$this {
    if (_$v != null) {
      _page = _$v.page;
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BaseProducts other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BaseProducts;
  }

  @override
  void update(void Function(BaseProductsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BaseProducts build() {
    _$BaseProducts _$result;
    try {
      _$result = _$v ?? new _$BaseProducts._(page: page, data: data.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BaseProducts', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
