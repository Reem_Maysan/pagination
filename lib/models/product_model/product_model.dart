library product_model;

import 'dart:convert';

import 'package:paginationapi/models/images_model/images_model.dart';
import 'package:paginationapi/serializer/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'product_model.g.dart';

abstract class ProductModel
    implements Built<ProductModel, ProductModelBuilder> {
  // fields go here

  int get id;

  @nullable
  String get title;

  @nullable
  String get image;

  @nullable
  BuiltList<ImagesModel> get images;

  ProductModel._();


  @nullable
  String get status;
  factory ProductModel([updates(ProductModelBuilder b)]) = _$ProductModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ProductModel.serializer, this));
  }

  static ProductModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProductModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProductModel> get serializer => _$productModelSerializer;
}
