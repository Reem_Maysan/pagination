// GENERATED CODE - DO NOT MODIFY BY HAND

part of images_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ImagesModel> _$imagesModelSerializer = new _$ImagesModelSerializer();

class _$ImagesModelSerializer implements StructuredSerializer<ImagesModel> {
  @override
  final Iterable<Type> types = const [ImagesModel, _$ImagesModel];
  @override
  final String wireName = 'ImagesModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ImagesModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'source',
      serializers.serialize(object.source,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  ImagesModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ImagesModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'source':
          result.source = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ImagesModel extends ImagesModel {
  @override
  final int id;
  @override
  final String source;

  factory _$ImagesModel([void Function(ImagesModelBuilder) updates]) =>
      (new ImagesModelBuilder()..update(updates)).build();

  _$ImagesModel._({this.id, this.source}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('ImagesModel', 'id');
    }
    if (source == null) {
      throw new BuiltValueNullFieldError('ImagesModel', 'source');
    }
  }

  @override
  ImagesModel rebuild(void Function(ImagesModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ImagesModelBuilder toBuilder() => new ImagesModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ImagesModel && id == other.id && source == other.source;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), source.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ImagesModel')
          ..add('id', id)
          ..add('source', source))
        .toString();
  }
}

class ImagesModelBuilder implements Builder<ImagesModel, ImagesModelBuilder> {
  _$ImagesModel _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _source;
  String get source => _$this._source;
  set source(String source) => _$this._source = source;

  ImagesModelBuilder();

  ImagesModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _source = _$v.source;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ImagesModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ImagesModel;
  }

  @override
  void update(void Function(ImagesModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ImagesModel build() {
    final _$result = _$v ?? new _$ImagesModel._(id: id, source: source);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
