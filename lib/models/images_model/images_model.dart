library images_model;

import 'dart:convert';

import 'package:paginationapi/serializer/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'images_model.g.dart';

abstract class ImagesModel implements Built<ImagesModel, ImagesModelBuilder> {
  // fields go here

  int get id;

  String get source;

  ImagesModel._();

  factory ImagesModel([updates(ImagesModelBuilder b)]) = _$ImagesModel;

  String toJson() {
    return json.encode(serializers.serializeWith(ImagesModel.serializer, this));
  }

  static ImagesModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ImagesModel.serializer, json.decode(jsonString));
  }

  static Serializer<ImagesModel> get serializer => _$imagesModelSerializer;
}
