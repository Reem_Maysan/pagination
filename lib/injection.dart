import 'package:get_it/get_it.dart';
import 'package:dio/dio.dart';
import 'package:paginationapi/data/repository/Irepository.dart';
import 'package:paginationapi/data/repository/repository.dart';
import 'package:paginationapi/ui/peoduct_content/bloc/product_bloc.dart';
import 'package:paginationapi/ui/products_page/bloc/products_bloc.dart';
import 'core/constent.dart';
import 'package:paginationapi/data/http_helper/http_helper.dart';
import 'package:paginationapi/data/http_helper/ihttp_helper.dart';
import 'package:paginationapi/data/db_helper/Idb_helper.dart';
import 'package:paginationapi/data/db_helper/db_helper.dart';
import 'package:paginationapi/data/prefs_helper/iprefs_helper.dart';
import 'package:paginationapi/data/prefs_helper/prefs_helper.dart';

final sl = GetIt.instance;

Future iniGetIt() async {
  sl.registerLazySingleton(() => ((Dio(BaseOptions(
      baseUrl: BaseUrl,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "charset": "utf-8",
        "Accept-Charset": "utf-8"
      },
      responseType: ResponseType.plain)))));

  // data

  sl.registerLazySingleton<IDbHelper>(() => DbHelper());
  sl.registerLazySingleton<IPrefsHelper>(() => PrefsHelper());
  sl.registerLazySingleton<IHttpHelper>(() => HttpHelper(sl()));
  sl.registerLazySingleton<IRepository>(() => Repository(sl(), sl(), sl()));

  /// ProductBloc

  sl.registerFactory(() => ProductsBloc(sl()));

  /// ProductBloc

  sl.registerFactory(() => ProductBloc(sl()));
}
