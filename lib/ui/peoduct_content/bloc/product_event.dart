library product_event;

import 'package:built_value/built_value.dart';

part 'product_event.g.dart';

abstract class ProductEvent {}

abstract class GetContent extends ProductEvent
    implements Built<GetContent, GetContentBuilder> {
  // fields go here

  int get id;

  GetContent._();

  factory GetContent([updates(GetContentBuilder b)]) = _$GetContent;
}

abstract class ClearError extends ProductEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}
