library product_state;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:paginationapi/models/product_model/product_model.dart';

part 'product_state.g.dart';

abstract class ProductState implements Built<ProductState, ProductStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;

  @nullable
  ProductModel get product;

  ProductState._();

  factory ProductState([updates(ProductStateBuilder b)]) = _$ProductState;

  factory ProductState.initail() {
    return ProductState((b) => b
      ..error = ""
      ..isLoading = false
      ..product = null);
  }
}
