// GENERATED CODE - DO NOT MODIFY BY HAND

part of product_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetContent extends GetContent {
  @override
  final int id;

  factory _$GetContent([void Function(GetContentBuilder) updates]) =>
      (new GetContentBuilder()..update(updates)).build();

  _$GetContent._({this.id}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('GetContent', 'id');
    }
  }

  @override
  GetContent rebuild(void Function(GetContentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetContentBuilder toBuilder() => new GetContentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetContent && id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(0, id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetContent')..add('id', id))
        .toString();
  }
}

class GetContentBuilder implements Builder<GetContent, GetContentBuilder> {
  _$GetContent _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  GetContentBuilder();

  GetContentBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetContent other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetContent;
  }

  @override
  void update(void Function(GetContentBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetContent build() {
    final _$result = _$v ?? new _$GetContent._(id: id);
    replace(_$result);
    return _$result;
  }
}

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
