import 'package:bloc/bloc.dart';
import 'package:paginationapi/data/repository/irepository.dart';
import 'package:paginationapi/ui/peoduct_content/bloc/product_event.dart';
import 'package:paginationapi/ui/peoduct_content/bloc/product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  IRepository _repository;

  ProductBloc(this._repository);

  @override
  ProductState get initialState => ProductState.initail();

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is GetContent) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..product = null);

        final data = await _repository.getProductContent(event.id);
        print('GetContent Success data $data');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..product.replace(data));
      } catch (e) {
        print('GetContent Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
          ..product = null);
      }
    }
  }
}
