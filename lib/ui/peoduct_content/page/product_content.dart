import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:paginationapi/core/constent.dart';
import 'package:paginationapi/core/custom_loader.dart';
import 'package:paginationapi/ui/peoduct_content/bloc/product_bloc.dart';
import 'package:paginationapi/ui/peoduct_content/bloc/product_event.dart';
import 'package:paginationapi/ui/peoduct_content/bloc/product_state.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../injection.dart';

class ProductContent extends StatefulWidget {
  int id;

  ProductContent({this.id});

  @override
  _ProductContentState createState() => _ProductContentState();
}

class _ProductContentState extends State<ProductContent> {
  final _bloc = sl<ProductBloc>();

  @override
  void initState() {
    _bloc.add(GetContent((b) => b..id = widget.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context, ProductState state) {
          error(state.error);

          return Scaffold(
            body: Stack(
              children: <Widget>[
                state.product != null
                    ? SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  height:
                                      MediaQuery.of(context).size.height / 2,
                                  child: PageView.builder(
                                      itemCount:
                                          state.product.images.length + 1,
                                      itemBuilder: (context, index) {
                                        return index == 0
                                            ? Container(
                                                color: Colors.black12,
                                                width: double.infinity,
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    2,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      15.0),
                                                  child: Center(
                                                    child: Image.network(
                                                      ImageUrl +
                                                          state.product.image,
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : Container(
                                                color: AppColor.containerColor,
                                                width: double.infinity,
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    2,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      15.0),
                                                  child: Center(
                                                    child: Image.network(
                                                      ImageUrl +
                                                          state
                                                              .product
                                                              .images[index - 1]
                                                              .source,
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ),
                                              );
                                      }),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    : Container(),
              ],
            ),
          );
        });
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.orangeAccent,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
