import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:paginationapi/core/constent.dart';
import 'package:paginationapi/core/custom_loader.dart';
import 'package:paginationapi/ui/peoduct_content/page/product_content.dart';
import 'package:paginationapi/ui/products_page/bloc/products_bloc.dart';
import 'package:paginationapi/ui/products_page/bloc/products_event.dart';
import 'package:paginationapi/ui/products_page/bloc/products_state.dart';

import '../../../injection.dart';

class Product extends StatefulWidget {
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  final _bloc = sl<ProductsBloc>();
  var _listController = ScrollController();

  @override
  void initState() {
    _bloc.add(GetProducts());
    _listController.addListener(() {
      if (_listController.position.atEdge) {
        if (_listController.position.pixels ==
            _listController.position.maxScrollExtent) {
          print('GetNext');
          _bloc.add(GetNext());
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context, ProductsState state) {
          error(state.error);
          return Scaffold(
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.orangeAccent,
              title: Text('Reemy'),
              centerTitle: true,
            ),
            body: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsetsDirectional.only(
                          start: 30.0, end: 30.0, top: 30),
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                            border: Border.all(
                              width: 2,
                              color: Colors.orangeAccent,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(30))),
                        child: TextField(
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                              color: Colors.orangeAccent, fontFamily: "book"),
                          onChanged: (text) {
                            setState(() {
                              //  query = text;
                            });
                            _bloc.add(GetProducts());
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                state.products.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsetsDirectional.only(top: 100.0),
                        child: GridView.count(
                          crossAxisCount: 2,
                          childAspectRatio: 0.90,
                          controller: _listController,
                          children:
                              List.generate(state.products.length, (index) {
                            return GestureDetector(
                              onTap: () {},
                              child: Center(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsetsDirectional.only(
                                        top: 15.0,
                                        start: 10,
                                        end: 10,
                                        bottom: 5),
                                    child: Stack(
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ProductContent(
                                                            id: state
                                                                .products[index]
                                                                .id)));
                                          },
                                          child: Container(
                                            height: 160,
                                            width: 165,
                                            decoration: BoxDecoration(
                                                color: Colors.orangeAccent,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                                image: DecorationImage(
                                                  image: NetworkImage(ImageUrl +
                                                      state.products[index]
                                                          .image),
                                                  fit: BoxFit.fill,
                                                )),
                                            child: GestureDetector(
                                              onTap: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ProductContent(
                                                                id: state
                                                                    .products[
                                                                        index]
                                                                    .id)));
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )),
                            );
                          }),
                        ),
                      )
                    : state.isLoading
                        ? Container(
                            child: Text('Loading..'),
                          )
                        : Container()
              ],
            ),
          );
        });
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.orangeAccent,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
