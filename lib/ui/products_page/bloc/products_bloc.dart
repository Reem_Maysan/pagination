import 'package:bloc/bloc.dart';
import 'package:paginationapi/data/repository/irepository.dart';
import 'products_event.dart';
import 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  IRepository _repository;

  ProductsBloc(this._repository);

  int currentPage = 1;
  int totalPage = 0;

  @override
  ProductsState get initialState => ProductsState.initail();

  @override
  Stream<ProductsState> mapEventToState(
    ProductsEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is AddListProducts) {
      yield state.rebuild((b) => b..products.replace(event.products));
    }
    if (event is GetNext) {
      currentPage++;
      if (currentPage <= totalPage) {
        yield state.rebuild((b) => b..isLoading = true);
        final data = await _repository.getProducts(currentPage);
        totalPage = data.page;
        yield state.rebuild((b) => b
          ..isLoading = false
          ..products.addAll(data.data));
      } else {
        yield state.rebuild((b) => b..isLoading = false);
      }
    }
    if (event is GetProducts) {
      currentPage = 1;
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..products.replace([]));

        final data = await _repository.getProducts(currentPage);

        totalPage = data.page;
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..products.replace(data.data));
      } catch (e) {
        print('GetProducts Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
          ..products.replace([]));
      }
    }
  }
}
