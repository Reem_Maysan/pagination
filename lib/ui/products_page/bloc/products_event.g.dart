// GENERATED CODE - DO NOT MODIFY BY HAND

part of products_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetProducts extends GetProducts {
  factory _$GetProducts([void Function(GetProductsBuilder) updates]) =>
      (new GetProductsBuilder()..update(updates)).build();

  _$GetProducts._() : super._();

  @override
  GetProducts rebuild(void Function(GetProductsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetProductsBuilder toBuilder() => new GetProductsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetProducts;
  }

  @override
  int get hashCode {
    return 583255550;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetProducts').toString();
  }
}

class GetProductsBuilder implements Builder<GetProducts, GetProductsBuilder> {
  _$GetProducts _$v;

  GetProductsBuilder();

  @override
  void replace(GetProducts other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetProducts;
  }

  @override
  void update(void Function(GetProductsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetProducts build() {
    final _$result = _$v ?? new _$GetProducts._();
    replace(_$result);
    return _$result;
  }
}

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$AddListProducts extends AddListProducts {
  @override
  final BuiltList<ProductModel> products;

  factory _$AddListProducts([void Function(AddListProductsBuilder) updates]) =>
      (new AddListProductsBuilder()..update(updates)).build();

  _$AddListProducts._({this.products}) : super._() {
    if (products == null) {
      throw new BuiltValueNullFieldError('AddListProducts', 'products');
    }
  }

  @override
  AddListProducts rebuild(void Function(AddListProductsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddListProductsBuilder toBuilder() =>
      new AddListProductsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddListProducts && products == other.products;
  }

  @override
  int get hashCode {
    return $jf($jc(0, products.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddListProducts')
          ..add('products', products))
        .toString();
  }
}

class AddListProductsBuilder
    implements Builder<AddListProducts, AddListProductsBuilder> {
  _$AddListProducts _$v;

  ListBuilder<ProductModel> _products;
  ListBuilder<ProductModel> get products =>
      _$this._products ??= new ListBuilder<ProductModel>();
  set products(ListBuilder<ProductModel> products) =>
      _$this._products = products;

  AddListProductsBuilder();

  AddListProductsBuilder get _$this {
    if (_$v != null) {
      _products = _$v.products?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddListProducts other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddListProducts;
  }

  @override
  void update(void Function(AddListProductsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddListProducts build() {
    _$AddListProducts _$result;
    try {
      _$result = _$v ?? new _$AddListProducts._(products: products.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'products';
        products.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AddListProducts', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GetNext extends GetNext {
  factory _$GetNext([void Function(GetNextBuilder) updates]) =>
      (new GetNextBuilder()..update(updates)).build();

  _$GetNext._() : super._();

  @override
  GetNext rebuild(void Function(GetNextBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetNextBuilder toBuilder() => new GetNextBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetNext;
  }

  @override
  int get hashCode {
    return 840588431;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetNext').toString();
  }
}

class GetNextBuilder implements Builder<GetNext, GetNextBuilder> {
  _$GetNext _$v;

  GetNextBuilder();

  @override
  void replace(GetNext other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetNext;
  }

  @override
  void update(void Function(GetNextBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetNext build() {
    final _$result = _$v ?? new _$GetNext._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
