import 'dart:convert';
import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:paginationapi/core/error.dart';
import 'package:paginationapi/models/base/base_response.dart';
import 'package:paginationapi/models/product_model/base_products.dart';
import 'package:paginationapi/models/product_model/product_model.dart';
import 'package:paginationapi/serializer/serializers.dart';

import 'ihttp_helper.dart';

class HttpHelper implements IHttpHelper {
  final Dio _dio;

  var url = "http://bikehub.store/api/customer/menus";

  var cookieJar = CookieJar();

  HttpHelper(this._dio) {
    _dio.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
    _dio.interceptors.add(CookieManager(cookieJar));
  }

  @override
  Future<ProductModel> getProductContent(int id) async {
    _dio.interceptors.add(CookieManager(cookieJar));

    try {
      String url = "";
      url = 'customer/store/product/$id';

      final response = await _dio.get(url);
      print('getProductContent Response StatusCode ${response.statusCode}');

      if (response.statusCode == 200) {
        print('getProductContent Response body  ${response.data}');

        final BaseResponse<ProductModel> baseResponse =
            serializers.deserialize(json.decode(response.data),
                specifiedType: FullType(
                  BaseResponse,
                  [
                    const FullType(ProductModel),
                  ],
                ));
        print("getProductContent status : $baseResponse");
        if (baseResponse.status == 200) {
          return baseResponse.data;
        } else {
          throw NetworkException();
        }
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e.toString());
      throw NetworkException();
    }
  }

  @override
  Future<BaseProducts> getProducts(int page) async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      String url = "";
      url = 'customer/products?&page=$page';

      final response = await _dio.get(url);
      print('getProducts Response StatusCode ${response.statusCode}');

      if (response.statusCode == 200) {
        print('getProducts Response body  ${response.data}');

        final BaseResponse<BaseProducts> baseResponse =
            serializers.deserialize(json.decode(response.data),
                specifiedType: FullType(
                  BaseResponse,
                  [
                    FullType(BaseProducts),
                  ],
                ));

        print("getProducts status : $baseResponse");
        if (baseResponse.status == 200) {
          return baseResponse.data;
        } else {
          throw NetworkException();
        }
      } else {
        throw NetworkException();
      }
    } catch (e) {
      throw NetworkException();
    }
  }
}
