
import 'package:paginationapi/models/product_model/base_products.dart';
import 'package:paginationapi/models/product_model/product_model.dart';

abstract class IHttpHelper {
  
  Future<BaseProducts> getProducts(int page);
  Future<ProductModel> getProductContent(int id);

}
