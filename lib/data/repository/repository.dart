import 'package:paginationapi/data/db_helper/Idb_helper.dart';
import 'package:paginationapi/data/http_helper/ihttp_helper.dart';
import 'package:paginationapi/data/prefs_helper/iprefs_helper.dart';
import 'package:paginationapi/models/product_model/base_products.dart';
import 'package:paginationapi/models/product_model/product_model.dart';

import 'Irepository.dart';

class Repository implements IRepository {
  IHttpHelper _ihttpHelper;
  IPrefsHelper _iprefHelper;
  IDbHelper _iDbHelper;

  Repository(this._ihttpHelper, this._iprefHelper, this._iDbHelper);

  @override
  Future<BaseProducts> getProducts(int page) async {
    return await _ihttpHelper.getProducts(page);
  }

  @override
  Future<ProductModel> getProductContent(int id) async {
    return await _ihttpHelper.getProductContent(id);
  }
}
