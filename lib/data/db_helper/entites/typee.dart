import 'package:floor/floor.dart';

/*
It will represent a database table as well as the scaffold of your business object. 
@entity marks the class as a persistent class. 
It's required to add a primary key to your table.
You can do so by adding the @primaryKey annotation to an int property.
There is no restriction on where you put the file containing the entity.
*/

@entity
class Typee {
  @primaryKey
  int id;

  String title;

  Typee({this.id, this.title});
}
