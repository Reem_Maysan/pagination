import 'package:floor/floor.dart';
import 'package:paginationapi/data/db_helper/entites/typee.dart';

/*
(Data Access Object)
This component is responsible for managing access to the underlying SQLite database. 
The abstract class contains the method signatures for
 querying the database which have to return a Future or Stream.
*/

@dao
abstract class TypeeDao {
  @insert
  Future<void> insertTypee(Typee type);

  @Query('SELECT * FROM Typee')
  Future<List<Typee>> getTypee();

  @Query('DELETE FROM Typee where id = :id')
  Future<void> deleteTypee(int id);
}

