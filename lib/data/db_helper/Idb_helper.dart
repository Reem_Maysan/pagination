import 'package:paginationapi/data/db_helper/entites/typee.dart';
import 'db/database.dart';

abstract class IDbHelper {
  
  Future<AppDatabase> _getInstDB() {}


  /// Typee
  Future<void> insertTypee(Typee type);

  Future<List<Typee>> getTypee();

  Future<void> deleteTypee(int id);
}
