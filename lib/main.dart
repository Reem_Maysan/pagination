import 'package:flutter/material.dart';
import 'package:paginationapi/ui/products_page/page/product.dart';

import 'injection.dart';

void main() async {
  await iniGetIt();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: Product(),
    );
  }
}

